# Лабораторная работа №5. Очереди сообщений на примере Kafka
## Альховский Константин МО-201, ОмГТУ

## Задание
- Запустить Kafka.
- Создать Topic.
- Написать два приложения на любом удобном языке программирования. Первое пишет
1000 сообщений - дата и время создания сообщения в Topic Kafka. Второе читает
сообщение из Topic и выводит поличество прочитанных сообщений на экран.

## Выполнение задания

- Установить _[Docker Desktop](https://www.docker.com/products/docker-desktop/)_
- Скачать архив с _[kafka](https://github.com/confluentinc/cli/releases/tag/v3.42.0)_
- Распаковать архив, перейти в папку _confluen_ через командную строку и выполнить команду
```sh
confluent local kafka start
```
- Запомнить _Plaintext Ports_ и записать его в файлы _consumer.py_ и _producer.py_
![Картинка портов](plaintext_ports.jpg)
- Создать _Kafka_ топик с названием _"quickstart"_ с помощью команды
```sh
confluent local kafka topic create quickstart
```
- Запустить из разных командных строк файлы _consumer.py_ и _producer.py_
- Наблюдать за выполнением программы
![Результат producer](producer.jpg)
![Результат consumer](consumer.jpg)