from kafka import KafkaConsumer
import time

def consume_messages(consumer, max_messages=1000, sleep_interval=0.1):
    message_count = 0
    for message in consumer:
        message_count += 1
        print(f"Количество прочитанных сообщений: {message_count}")
        if message_count >= max_messages:
            break
        time.sleep(sleep_interval)

def main():
    bootstrap_servers = 'localhost:54033'
    topic = 'quickstart'
    consumer = KafkaConsumer(topic, bootstrap_servers=bootstrap_servers)
    consume_messages(consumer)
    consumer.close()

if __name__ == "__main__":
    main()