from kafka import KafkaProducer
from datetime import datetime
import time

def produce_messages(producer, topic, num_messages=1000, sleep_interval=0.1):
    for i in range(num_messages):
        message = f"Сообщение {i + 1}: {datetime.now()}"
        print(message)
        producer.send(topic, value=message.encode('utf-8'))
        time.sleep(sleep_interval)

def main():
    bootstrap_servers = 'localhost:54033'
    topic = 'quickstart'
    producer = KafkaProducer(bootstrap_servers=bootstrap_servers)
    produce_messages(producer, topic)
    producer.close()

if __name__ == "__main__":
    main()

